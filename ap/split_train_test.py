#!/usr/bin/env python
import codecs
import sys
import random

def split(argv):
    data = codecs.open(argv[0], 'r', 'utf-8')
    output = codecs.open(argv[1], 'w', 'utf-8')

    id = 0
    for line in data:
        line = line.strip()
        terms = line.split()
        nline = []
        for i in range(1, len(terms)):
            term = terms[i]
            blocks = term.split(":")
            for w in range(0, int(blocks[1])):
                nline.append(blocks[0])

        random.shuffle(nline)
        output.write(str(id) + ":" + str(len(nline)) +" " + ' '. join(nline) + "\n")
        id += 1
    output.flush()
    output.close()



if __name__ == "__main__":
    print len(sys.argv)
    if len(sys.argv) < 2:
        print "data, output"
        sys.exit(0)

    split(sys.argv[1:])
