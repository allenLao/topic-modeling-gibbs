//
//  dirichlet_estimation.cpp
//  multi-lingual-topic-modeling
//
//  Created by allen on 4/18/13.
//  Copyright (c) 2013 allen. All rights reserved.
//

#include "dirichlet_estimation.h"


double alhood(double a, double ss, size_t D, size_t K)
{ return(D * (lgamma(K * a) - K * lgamma(a)) + (a - 1) * ss); }

double d_alhood(double a, double ss, size_t D, size_t K)
{ return(D * (K * digamma(K * a) - K * digamma(a)) + ss); }

double d2_alhood(double a, size_t D, size_t K)
{ return(D * (K * K * trigamma(K * a) - K * trigamma(a))); }


/**
 *a general functions to
 *estimate the alphas
 *cf. LDA. A.4.2
 *
 */
double a_log_lhood(double* alpha, double* ss, size_t D, size_t K){
    double lik = 0;
    double asum =0;
    int k;
    for(k =0; k < K; k++)
    {
        lik -= D * lgamma(alpha[k]) + (alpha[k] -1) * ss[k];
        asum += alpha[k];
    }
    lik += lgamma(asum) * D;
    
    return ( lik );
}


void d_log_lhood(double* a, double *ss, double* grad, size_t D, size_t K)
{
    double sum = sum_up(a, K);
    int k;
    for(k =0; k < K; k++)
    {
        grad[k] = (digamma(sum) - digamma(a[k]))*D + ss[k];
    }
}

double d2_log_lhood(double* a, double* ss, double* diag, size_t D, size_t K){
    double sum_trigamma = trigamma(sum_up(a, K)) * D;
    int k;
    for(k =0; k < K; k++)
    {
        diag[k] = -trigamma(a[k])*D;
    }
    return sum_trigamma;
    
}

void newton_est_alpha(double* alpha, double* ss, size_t D, size_t K){
    newton_est_alpha_weight(alpha, ss, D, K, 1.0);
}

void newton_est_alpha_weight(double* alpha, double* ss, size_t D, size_t K, double roh)
{
    double grad[K];
    double q[K];
    double b, z, num = 0, deno =0, lik =200, olik =10;
    int iter = 0, k;
    
    while((fabs(lik - olik)/fabs(olik) > NEWTON_THRESH) && (iter < MAX_ALPHA_ITER))
    {
        //current likelihood
        olik = a_log_lhood(alpha, ss, D, K);
        //first order derivative
        d_log_lhood(alpha, ss, grad, D, K);
        //second order derivative
        z = d2_log_lhood(alpha, ss, q, D, K);
        num = 0;
        deno = 0;
        for(k =0; k < K; k++)
        {
            num += grad[k]/q[k];
            deno += 1.0/q[k];
        }
        deno += 1.0/z;
        b = num/deno;
        for(k =0; k < K; k++)
        {
            alpha[k] -= roh * (grad[k] - b)/q[k];
        }
        lik = a_log_lhood(alpha, ss, D, K);
        iter++;
    }
    
}

/*
 * newtons method
 *
 */

double opt_alpha(double ss, size_t D, size_t K, double roh)
{
    double a, log_a, init_a = 100;
    double f, df, d2f;
    int iter = 0;
    
    log_a = log(init_a);
    do
    {
        iter++;
        a = exp(log_a);
        if (isnan(a))
        {
            init_a = init_a * 10;
            printf("warning : alpha is nan; new init = %5.5f\n", init_a);
            a = init_a;
            log_a = log(a);
        }
        f = alhood(a, ss, D, K);
        df = d_alhood(a, ss, D, K);
        d2f = d2_alhood(a, D, K);
        log_a = log_a - roh * df/(d2f * a + df);
        //printf("alpha maximization : %5.5f   %5.5f\n", f, df);
    }
    while ((fabs(df) > NEWTON_THRESH) && (iter < MAX_ALPHA_ITER));
    return(exp(log_a));
}
