//
//  dirichlet_estimation.h
//  multi-lingual-topic-modeling
//
//  Created by allen on 4/18/13.
//  Copyright (c) 2013 allen. All rights reserved.
//

#ifndef __multi_lingual_topic_modeling__dirichlet_estimation__
#define __multi_lingual_topic_modeling__dirichlet_estimation__

#include "utils.h"
#define NEWTON_THRESH 1e-5
#define MAX_ALPHA_ITER 1000

double alhood(double, double, size_t, size_t);
double d_alhood(double, double, size_t, size_t);
double d2_alhood(double, size_t, size_t);
double opt_alpha(double, size_t, size_t, double);

double a_log_lhood(double*, double*, size_t, size_t);
void d_log_lhood(double*, double*, double*, size_t, size_t);
double d2_log_lhood(double*, double*, double*, size_t, size_t);
void newton_est_alpha(double*, double*, size_t, size_t);
void newton_est_alpha_weight(double*, double*, size_t, size_t, double);

#endif /* defined(__multi_lingual_topic_modeling__dirichlet_estimation__) */
