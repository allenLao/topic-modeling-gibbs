//
//  mlda-gibbs.cpp
//  topic-model-gibbs
//
//  Created by allen on 10/4/13.
//  Copyright (c) 2013 allen. All rights reserved.
//

#include "mlda-gibbs.h"

void MLDA_GIBBS::estimate()
{
    int t, k, w, l, it = 0, nf=0, lang;
    char prefix[100];
    char likes_file[100];
    sprintf(likes_file, "%s/mlda_%d_likelihood.liks",root, nTopic);
    FILE* likptr = fopen(likes_file, "w");
    time_t begin = time(0);
    std::cout<<"***************Begin to MLDA-Gibbs **************\n";
    std::cout<<"Begin with:"<<ctime(&begin)<<"\n";
    while (it < nIteration)
    {
        time_t lbegin = time(0);
        
        for (t = 0; t < train->size(); t++)
        {
            Tuple* tuple = train->tuple(t);
            for (l = 0; l< tuple->size(); l++)
            {
                Document doc = tuple->get(l);
                lang = doc.language();
                for (w = 0; w < doc.size(); w++)
                {
                    k = sampling(lang, l, t, w, false);
                    topic_words[lang][k][doc.word(w)]++;
                    sum_topic_words[lang][k] ++;
                    dtopics[t][k]++;
                    states[t][lang][w] = k;
                }
            }
        }
        time_t lend = time(0);
        std::cout<<"iteration "<<(it+1)<<"-th: "<<difftime(lend, lbegin)<<" seconds"<<std::endl;
        
        if ((it > nBurnin) && (it % nLag == 0))
        {
            // dump model
            update();
            sprintf(prefix, "%04d", nf);
            save(prefix);
            save_state();
            nf++;
        }
        if (test != NULL)
            holdout_liks();
        
        if ( (test != NULL) &&(it % nInterval == 0)) {
            time_t hend = time(0);
            //write down hold-out likelihood
            double liks = holdout_liks();
            fprintf(likptr, "%f %f", liks, difftime(hend, begin));
            fflush(likptr);
            std::cout<<liks<<" "<<difftime(hend, begin)<<std::endl;
        }
        it++;
    }
    
    update();
    sprintf(prefix, "final");
    save_state();
    save(prefix);
    fflush(likptr);
    fclose(likptr);
    time_t end = time(0);
    std::cout<<"It takes around "<<difftime(begin, end) / 60<<" mins.\n";
    std::cout<<"End with:"<<ctime(&end)<<"\n";
    std::cout<<"***************End MLDA-Gibbs **************\n";
    
}

/*
 *compute holdout likelihoods
 */
double MLDA_GIBBS::holdout_liks()
{
    if (test != NULL)
    {
        double likes = 0;
        int count = 0;
        int d = 0;
        //compute beta and theta
        for (d = 0; d <test->size(); d++)
        {
            likes += inference(d);
            count += test->tuple(d)->words();
        }
        
        return log(likes/count);
    }
    return  0;
}


/*
 *Sample a topic for a word, language code
 */
int MLDA_GIBBS::sampling(int lid, int l, int d, int w, bool btest)
{
    int k;
    Tuple* tuple;
    Document doc;
    int wid;
    int ct;
    double score[nTopic];
    if (!btest)
    {
        tuple = train->tuple(d);
        doc = tuple->get(l);
        wid = doc.word(w);
        ct = states[d][lid][w];
        topic_words[lid][ct][wid]--;
        sum_topic_words[lid][ct]--;
        dtopics[d][ct]--;
        for (k = 0; k < nTopic; k++)
        {
            score[k] = (dtopics[d][k] + alpha)*(topic_words[lid][k][wid]+eta);
            score[k] /= (sum_topic_words[lid][k] + nVocab[lid] * eta);
        }
    }
    else
    {
        tuple = test->tuple(d);
        doc = tuple->get(l);
        wid = doc.word(w);
        ct = test_states[d][lid][w];
        test_dtopics[d][ct]--;
        for (k = 0; k < nTopic; k++)
        {
            score[k] = (test_dtopics[d][k] + alpha)*(topic_words[lid][k][wid]+eta);
            score[k] /= (sum_topic_words[lid][k] + nVocab[lid] * eta);
        }
    }
    //cumulate scores
    for (k = 1; k < nTopic; k++)
        score[k] = score[k-1] + score[k];
    //random select
    double rdm = myrand()*score[nTopic-1];
    k = 0;
    for (k = 0; k < nTopic; k++)
        if (rdm < score[k])
            break;
    return k;
}

//discard the words not in training data
double MLDA_GIBBS::inference(int t)
{
    int l, w, k, count;
    double lik;
    double local_theta[nTopic];
    
    Tuple* tuple = test->tuple(t);
    
    
    count =0;
    for (l = 0; l <tuple->size(); l++)
    {
        Document doc = tuple->get(l);
        int lang = doc.language();
        for (w = 0; w <doc.size(); w++)
        {
            int ct = test_states[t][lang][w];
            if (ct > -1)
            {
                count++;
                k = sampling(doc.language(), l, t, w, true);
                test_states[t][lang][w] =k;
                test_dtopics[t][k]++;
            }
        }
    }
    
    for (k = 0; k < nTopic; k++)
        local_theta[k] = (test_dtopics[t][k] + alpha)/(count + alpha*nTopic);
    lik = 0;
    for (l = 0; l < tuple->size(); l++)
    {
        Document doc = tuple->get(l);
        for (w = 0; w < doc.size(); w++)
        {
            for (k = 0; k < nTopic; k++)
                lik +=(topic_words[doc.language()][k][doc.word(w)] +eta)/(eta*nVocab[doc.language()] + sum_topic_words[doc.language()][k])* local_theta[k];
        }
    }
    
    return (lik);
}

void MLDA_GIBBS::update()
{
    int d, w, k, l;
    for (l = 0; l < nLang; l++)
        for (k = 0; k <nTopic; k++)
            for (w = 0; w < nVocab[l]; w++)
                beta[l][k][w] = (topic_words[l][k][w] + eta)/(sum_topic_words[l][k] + nVocab[l]*eta);
    
    for (d = 0; d < train_docs(); d++)
        for (k =0; k <nTopic; k++)
            theta[d][k] = (dtopics[d][k] + alpha)/(train->tuple(d)->words() + nTopic * alpha);
}

void MLDA_GIBBS::save_state()
{
    int d, w, l;
    char cst[nLang][100];
    FILE* fptr[nLang];
    for (l = 0; l< nLang; l++)
    {
        sprintf(cst[l], "%s/assignment_lang_%d.bat",root, l);
        fptr[l] = fopen(cst[l], "w");
    }
    
    for (d = 0; d < train_docs(); d++)
    {
        Tuple* tuple = train->tuple(d);
        for (l = 0; l < tuple->size(); l++)
        {
            Document doc = tuple->get(l);
            int lang = doc.language();
            fprintf(fptr[lang], "%d:%d ", doc.docid(),doc.size());
            for (w = 0; w < doc.size(); w++)
            {
                fprintf(fptr[lang], "%d ", states[d][lang][w]);
            }
            fprintf(fptr[lang], "\n");
            fflush(fptr[lang]);
            
        }
    }
    for (l = 0; l < nLang; l++)
    {
        fflush(fptr[l]);
        fclose(fptr[l]);
    }
    
    if (test != NULL) {
        for (l = 0; l< nLang; l++)
        {
            sprintf(cst[l], "%s/test_assignment_lang_%d.bat",root, l);
            fptr[l] = fopen(cst[l], "w");
        }
        
        for (d = 0; d < test_docs(); d++)
        {
            Tuple* tuple = test->tuple(d);
            for (l = 0; l < tuple->size(); l++)
            {
                Document doc = tuple->get(l);
                int lang = doc.language();
                fprintf(fptr[lang], "%d:%d ", doc.docid(),doc.size());
                for (w = 0; w < doc.size(); w++)
                {
                    fprintf(fptr[lang], "%d ", test_states[d][lang][w]);
                }
                fprintf(fptr[lang], "\n");
                fflush(fptr[lang]);
                
            }
        }
        for (l = 0; l < nLang; l++)
        {
            fflush(fptr[l]);
            fclose(fptr[l]);
        }
    }
    
}

void MLDA_GIBBS::save(const char *prefix)
{
    int l, k, w, d;
    char beta_file[100], theta_file[100];
    FILE* betaptr, *thetaptr;
    sprintf(theta_file, "%s/%s.theta",root, prefix);
    thetaptr = fopen(theta_file, "w");
    
    //keep topic-word distributions
    for (l = 0; l < nLang; l++)
    {
        sprintf(beta_file, "%s/%s_language_%d.beta",root, prefix, l);
        betaptr = fopen(beta_file, "w");
        
        for (k = 0; k < nTopic; k++)
        {
            for (w = 0; w < nVocab[l]; w++)
            {
                fprintf(betaptr, "%f ", beta[l][k][w]);
            }
            fprintf(betaptr, "\n");
        }
        fflush(betaptr);
        fclose(betaptr);
    }
    
    for (d = 0; d < train->size(); d++)
    {
        for (k = 0; k < nTopic; k++)
        {
            fprintf(thetaptr, "%f ", theta[d][k]);
        }
        fprintf(thetaptr, "\n");
    }
    fflush(thetaptr);
    fclose(thetaptr);
}

void MLDA_GIBBS::init()
{
    int k, w, d, l;
    for (l = 0; l < nLang; l++)
    {
        nSize[l] = train_docs();
        nVocab[l] = train->types(l);
        nTotal[l] = train->total(l);
        nMax [l]  = train->maxs(l);
        if (test!=NULL)
        {
            nTestTotal[l] = test->total(l);
            nTestMax[l] = test->maxs(l);
        }
    }
    
    //zero topic-word counts
    for (l = 0; l < nLang; l++)
    {
        std::vector<std::vector<int> > ltopic;
        std::vector<std::vector<double> > lbeta;
        std::vector<int> stopic(nTopic, 0);
        sum_topic_words.push_back(stopic);
        for (k = 0; k < nTopic; k++)
        {
            std::vector<int> swt(nVocab[l], 0);
            ltopic.push_back(swt);
            std::vector<double> sbt(nVocab[l], 0.0);
            lbeta.push_back(sbt);
        }
        topic_words.push_back(ltopic);
        sum_topic_words.push_back(stopic);
        beta.push_back(lbeta);
    }
    
    //random initialize
    
    for (d = 0; d < train->size(); d++)
    {
        std::vector<std::vector<int> > dls;
        std::vector<int> dds(nTopic, 0); //local topic count
        std::vector<double> ddt(nTopic, 0.0); //local theta
        
        Tuple* tuple = train->tuple(d);
        for (l =0; l < nLang; l++)
        {
            std::vector<int> lls;
            dls.push_back(lls);
        }
        
        for (l =0; l < train->tuple(d)->size(); l++)
        {
            Document doc = tuple->get(l);
            int lang = doc.language();
            dls[lang].resize(doc.size(), 0);
        }
        states.push_back(dls);
        dtopics.push_back(dds);
        theta.push_back(ddt);
        for (l =0; l < train->tuple(d)->size(); l++)
        {
            Document doc = tuple->get(l);
            int lang = doc.language();
            for (w = 0; w < doc.size(); w++)
            {
                int t =(int) (myrand() * nTopic);
                states[d][lang][w] = t;
                topic_words[lang][t][doc.word(w)]++;
                sum_topic_words[lang][t]++;
                dtopics[d][t]++;
            }
        }
    }
    update();
    
    if (test!=NULL)
    {
        for (d = 0; d < test->size(); d++)
        {
            Tuple* ttuple = test->tuple(d);
            std::vector<int> ttd(nTopic, 0);
            test_dtopics.push_back(ttd);
            std::vector<std::vector<int> > dls;
            
            for (l =0; l < nLang; l++)
            {
                std::vector<int> lls;
                dls.push_back(lls);
            }
            
            for (l = 0; l <ttuple->size(); l++)
            {
                Document doc = ttuple->get(l);
                int lans = doc.language();
                dls[lans].resize(doc.size(), 0);
            }
            test_dtopics.push_back(ttd);
            test_states.push_back(dls);
            for (l = 0; l <ttuple->size(); l++)
            {
                Document doc = ttuple->get(l);
                int lans = doc.language();
                for (w = 0; w < doc.size(); w++)
                {
                    if (doc.word(w) >= nVocab[doc.language()])
                        test_states[d][lans][w] = -1;
                    int t =(int) (myrand() * nTopic);
                    test_dtopics[d][t]++;
                    test_states[d][lans][w] = t;
                }
            }
        }
    }
}

MLDA_GIBBS::MLDA_GIBBS(int k, int s, int bn, int lg, int itv, double ap,
           double et, char* rt, MLCorpus* cp, MLCorpus* tt)
{
    nTopic = k;
    nIteration = s;
    nBurnin = bn;
    nLag = lg;
    nInterval = itv;
    alpha = ap;
    eta = et;
    train = cp;
    nLang = cp->languages();
    test = tt;
    sprintf(root, "%s", rt);
    nSize.resize(nLang, 0);
    nVocab.resize(nLang, 0);
    nTotal.resize(nLang, 0);
    nMax.resize(nLang, 0);
    nTestMax.resize(nLang, 0);
    nTestSize.resize(nLang, 0);
    nTestTotal.resize(nLang, 0);
    init();
}
MLDA_GIBBS::~MLDA_GIBBS()
{
    int d, k, l;
    //release mems
    for (l = 0; l < nLang; l++)
    {
        for (k = 0; k < nTopic; k++)
        {
            beta[l][k].clear();
            topic_words[l][k].clear();
        }
        beta[l].clear();
        topic_words[l].clear();
        sum_topic_words[l].clear();
    }
    beta.clear();
    topic_words.clear();
    sum_topic_words.clear();
    //
    for (d = 0; d < train_docs(); d++)
    {
        for(l = 0; l < train->tuple(d)->size(); l++)
            states[d][l].clear();
        dtopics[d].clear();
        states[d].clear();
        theta[d].clear();
    }
    states.clear();
    dtopics.clear();
    theta.clear();
    if(test != NULL)
    {
        for (d = 0; d < test_docs(); d++)
        {
            test_dtopics[d].clear();
            test_states[d].clear();
        }
        test_dtopics.clear();
        test_states.clear();
    }
    nSize.clear();
    nVocab.clear();
    nTotal.clear();
    nMax.clear();
    nTestMax.clear();
    nTestSize.clear();
    nTestTotal.clear();
}
