//
//  lda-gibbs.cpp
//  topic-modeling
//
//  Created by allen on 9/23/13.
//  Copyright (c) 2013 allen. All rights reserved.
//

#include "lda-gibbs.h"

void LDA_GIBBS::estimate()
{
    int d, k, w, it = 0, nf=0;
    char prefix[100];
    char likes_file[100];
    sprintf(likes_file, "%s/corrlda_%d_likelihood.liks",root, nTopic);
    FILE* likptr = fopen(likes_file, "w");
    time_t begin = time(0);
    std::cout<<"***************Begin to LDA-Gibbs **************\n";
    std::cout<<"Begin with:"<<ctime(&begin)<<"\n";
    while (it < nIteration)
    {
        time_t lbegin = time(0);
        
        for (d = 0; d < corpus->size(); d++)
        {
            Document* doc = corpus->get(d);
            for (w = 0; w < doc->size(); w++)
            {
                k = sampling(d, w);
                //update parameters
                topic_words[k][doc->word(w)] ++;
                sum_topic_words[k] ++;
                dtopics[d][k] ++;
                states[d][w] = k;
            }
        }
        time_t lend = time(0);
        std::cout<<"iteration "<<it<<"-th: "<<difftime(lend, lbegin)<<" seconds"<<std::endl;
        if ((it > nBurnin) && (it % nLag == 0))
        {
            // dump model
            update();
            sprintf(prefix, "%04d", nf);
            save_state();
            save(prefix);
            nf++;
        }
        if (test != NULL)
            holdout_liks();
        
        if ( (test != NULL) && (it % nInterval == 0)) {
            time_t hend = time(0);
            //write down hold-out likelihood
            double liks = holdout_liks();
            fprintf(likptr, "%f %f", liks, difftime(hend, begin));
            fflush(likptr);
            std::cout<<liks<<" "<<difftime(hend, begin)<<std::endl;
        }
        it++;
    }
    
    update();
    sprintf(prefix, "final");
    save_state();
    save(prefix);
    fflush(likptr);
    fclose(likptr);
    time_t end = time(0);
    std::cout<<"It takes around "<<difftime(begin, end)<<" mins.\n";
    std::cout<<"End with:"<<ctime(&end)<<"\n";
    std::cout<<"***************End LDA-Gibbs **************\n";
}

double LDA_GIBBS::holdout_liks()
{
    if (test != NULL)
    {
        double likes = 0;
        int d = 0;
        //compute beta and theta
        for (d = 0; d <test->size(); d++)
            likes += inference(d);
        return log(likes/test->words());
    }
    return  0;
}



int LDA_GIBBS::sampling(int d, int w)
{
    int k;
    int wid = corpus->get(d)->word(w);
    int ct = states[d][w];
    double score[nTopic];
    topic_words[ct][wid]--;
    sum_topic_words[ct]--;
    dtopics[d][ct]--;
    
    for (k = 0; k < nTopic; k++) {
        score[k] = (dtopics[d][k] + alpha)*(topic_words[k][wid]+eta);
        score[k] /= (sum_topic_words[k] + nVocabuary * eta);
    }
    //cumulate scores
    for (k = 1; k<nTopic; k++)
        score[k] = score[k-1] + score[k];
    
    //random select
    double rdm = myrand()*score[nTopic-1];
    for (k = 0; k < nTopic; k++)
    {
        if (rdm < score[k])
            break;
    }
    return k;
}

//discard the words not in training data
double LDA_GIBBS::inference(int d)
{
    int w, k, count;
    double lik;
    double score[nTopic], local_theta[nTopic];
    
    Document* doc = test->get(d);
    
    count =0;
    for (w = 0; w <doc->size(); w++)
    {
        int ct = test_states[d][w];
        if (ct > -1)
        {
            count++;
            test_dtopics[d][ct]--;
            for (k = 0; k < nTopic; k++)
            {
                score[k] = (test_dtopics[d][k] + alpha)
                *(topic_words[k][doc->word(w)] +eta)
                /(eta*nVocabuary + sum_topic_words[k]);
            }
            //cumulate scores
            for (k = 1; k<nTopic; k++)
                score[k] = score[k-1] + score[k];
            //random select
            double rdm = myrand()*score[nTopic-1];
            for (k = 0; k < nTopic; k++)
            {
                if (rdm < score[k])
                    break;
            }
            test_states[d][w] =k;
            test_dtopics[d][k]++;
        }
    }
    for (k = 0; k < nTopic; k++)
        local_theta[k] = (test_dtopics[d][k] + alpha)/(count + alpha*nTopic);
    lik = 0;
    for (w = 0; w < doc->size(); w++)
    {
        for (k = 0; k < nTopic; k++)
            lik +=(topic_words[k][doc->word(w)] +eta)/(eta*nVocabuary + sum_topic_words[k])* local_theta[k];
    }
    return (lik);
}

void LDA_GIBBS::update()
{
    int d, w, k;
    for (k = 0; k <nTopic; k++)
    {
        for (w = 0; w < nVocabuary; w++)
            beta[k][w] = (topic_words[k][w] + eta)/(sum_topic_words[k] + nVocabuary*eta);
    }
    
    for (d = 0; d < corpus->size(); d++)
    {
        for (k =0; k <nTopic; k++)
            theta[d][k] = (dtopics[d][k] + alpha)/(corpus->get(d)->size() + nTopic * alpha);
    }
}

void LDA_GIBBS::save_state()
{
    int d, w;
    char cst[100];
    sprintf(cst, "%s/train_assignment.bat",root);
    FILE* fptr;
    fptr = fopen(cst, "w");
    for (d = 0; d < corpus->size(); d++)
    {
        for (w = 0; w < corpus->get(d)->size(); w++)
            fprintf(fptr, "%d ", states[d][w]);
        fprintf(fptr, "\n");
    }
    fflush(fptr);
    fclose(fptr);
    if (test != NULL) {
        sprintf(cst, "%s/test_assignment.bat",root);
        fptr = fopen(cst, "w");
        for (d = 0; d < test->size(); d++)
        {
            for (w = 0; w < test->get(d)->size(); w++)
                fprintf(fptr, "%d ", states[d][w]);
            fprintf(fptr, "\n");
        }
        fflush(fptr);
        fclose(fptr);
    }
    
}

void LDA_GIBBS::save(const char *prefix)
{
    int k, w, d;
    char beta_file[100], theta_file[100];
    FILE* betaptr, *thetaptr;
    sprintf(beta_file, "%s/%s.beta",root, prefix);
    sprintf(theta_file, "%s/%s.theta",root, prefix);
    betaptr = fopen(beta_file, "w");
    thetaptr = fopen(theta_file, "w");
    
    //keep topic-word distributions
    for (k = 0; k < nTopic; k++)
    {
        for (w = 0; w < nVocabuary; w++)
            fprintf(betaptr, "%f ", beta[k][w]);
        fprintf(betaptr, "\n");
    }
    fflush(betaptr);
    fclose(betaptr);
    for (d = 0; d < corpus->size(); d++)
    {
        for (k = 0; k < nTopic; k++)
            fprintf(thetaptr, "%f ", theta[d][k]);
        fprintf(thetaptr, "\n");
    }
    fflush(thetaptr);
    fclose(thetaptr);
}

void LDA_GIBBS::init()
{
    int k, w, d;
    
    sum_topic_words.resize(nTopic, 0.0);
    //zero topic-word counts
    for (k = 0; k < nTopic; k++)
    {
        std::vector<int> twod(nVocabuary, 0);
        topic_words.push_back(twod);
        std::vector<double> tbeta(nVocabuary, 0.0);
        beta.push_back(tbeta);
    }
    
    //random initialize
    for (d = 0; d < corpus->size(); d++)
    {
        std::vector<int> dt(nTopic, 0);
        std::vector<double> tt(nTopic, 0.0);
        theta.push_back(tt);
        dtopics.push_back(dt);
        
        Document* doc = corpus->get(d);
        std::vector<int> ss(doc->size(), 0);
        states.push_back(ss);
        for (w = 0; w < doc->size(); w++)
        {
            int t =(int) (myrand() * nTopic);
            states[d][w] = t;
            topic_words[t][doc->word(w)]++;
            sum_topic_words[t]++;
            dtopics[d][t]++;
        }
    }
    update();
    
    if (test!=NULL)
    {
        
        for (d = 0; d < test->size(); d++)
        {
            std::vector<int> ttd(nTopic, 0);
            test_dtopics.push_back(ttd);
            
            Document* doc = test->get(d);
            std::vector<int> tss(doc->size(), 0);
            test_states.push_back(tss);
            for (w = 0; w < doc->size(); w++)
            {
                if (doc->word(w) >= nVocabuary)
                    test_states[d][w] = -1;
                int t =(int) (myrand() * nTopic);
                test_dtopics[d][t]++;
                test_states[d][w] = t;
            }
        }
    }
}

LDA_GIBBS::LDA_GIBBS(int k, int s, int bn, int lg, int itv, double ap,
          double et, char* rt, Corpus* cp, Corpus* tt)
{
    nTopic = k;
    nIteration = s;
    nBurnin = bn;
    nLag = lg;
    nInterval = itv;
    alpha = ap;
    eta = et;
    corpus = cp;
    nVocabuary = cp->types();
    nDocument = cp->size();
    test = tt;
    sprintf(root, "%s", rt);
    init();
}


