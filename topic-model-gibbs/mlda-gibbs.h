//
//  mlda-gibbs.h
//  topic-model-gibbs
//
//  Created by allen on 10/4/13.
//  Copyright (c) 2013 allen. All rights reserved.
//

#ifndef __topic_model_gibbs__mlda_gibbs__
#define __topic_model_gibbs__mlda_gibbs__

#include <ctime>
#include <stdlib.h>
#include <iostream>
#include "data.h"
#include "utils.h"
#include "cokus.h"

#define myrand() (double) (((unsigned long) randomMT()) / 4294967296.)

class MLDA_GIBBS
{
    int nTopic; //topic
    int nIteration; //iteration
    int nBurnin; //throw away first nBurnin iterations
    int nLag; // freq to dump model
    int nInterval; //interval of compute hold-out likelihood
    int nLang;
    int testMax;
    std::vector<int> nSize;
    std::vector<int> nVocab;
    std::vector<int> nTotal;
    std::vector<int> nMax;
    std::vector<int> nTestMax;
    std::vector<int> nTestSize;
    std::vector<int> nTestTotal;
    
    double alpha;
    double eta;
    char root[100];
    
    //language * topic * words
    std::vector<std::vector<std::vector<int> > > topic_words;
    //language * topic
    std::vector<std::vector<int> > sum_topic_words;
    //topic-word distribution
    std::vector<std::vector<std::vector<double> > > beta;
    
    // topic states, tuple * language * words
    std::vector<std::vector<std::vector<int> > > states;
    //tuple * topics
    std::vector<std::vector<int> > dtopics;
   
    //document propotion
    std::vector<std::vector<double> > theta;
    MLCorpus* train;
    
    //test data
    MLCorpus* test;
    std::vector<std::vector<std::vector<int> > > test_states;
    std::vector<std::vector<int> > test_dtopics;
    
public:
    MLDA_GIBBS(int, int, int, int, int, double,
               double, char*, MLCorpus*, MLCorpus*);
    ~MLDA_GIBBS();
    int train_docs(){ return train->size();}
    int test_docs() {return test->size();}
    int vocab(int l){ return nVocab[l];}
    //given a d-th document and w-th word in it,
    //sample a new topic assignment
    int sampling(int lid, int l, int d, int w, bool btest);
    //inference topic given current model
    double inference(int d);
    void estimate();
    void update();
    double holdout_liks();
    double likelihood();
    void init();
    void save(const char* prefix);
    void save_state();
};

#endif /* defined(__topic_model_gibbs__mlda_gibbs__) */
