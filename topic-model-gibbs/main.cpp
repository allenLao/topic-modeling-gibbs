//
//
//  Created by allen
//  Copyright (c) 2013 allen. All rights reserved.
//

#include "main.h"

void print_usage()
{
    std::cout<<"************** GIBBS SAMPLING PACKAGE *************"<<std::endl;
    std::cout<<"\n===========Usage of LDA==========="<<std::endl;
    std::cout<<"./gbs lda [topics] [iteration] [burn-in] [lag] [interval] [alpha] [eta] [root directory] [data]\n"<<std::endl;
    std::cout<<"\n===========Usage of MLDA=========="<<std::endl;
    std::cout<<"./gbs mlda [topics] [iteration] [burn-in] [lag] [interval] [alpha] [eta] [root directory] [language] [data_1] [data_2] ... [test_data_1] ...\n"<<std::endl;
    
    std::cout<<"\n===========Usage of CORR-LDA=========="<<std::endl;
    std::cout<<"./gbs corr-lda [topics] [iteration] [burn-in] [lag] [interval] [alpha] [eta] [root directory] [language] [data_1] [data_2] ... [test_data_1] ...\n"<<std::endl;
    
    std::cout<<"\n===========Usage of PLDA=========="<<std::endl;
    std::cout<<"./gbs plda [topics] [function] [iteration] [burn-in] [lag] [interval] [alpha] [eta] [lambda] [root directory] [language] [data_1] [data_2] ... [test_data_1] ..."<<std::endl;
    std::cout<<"Function:default is js; 1 is hellinger; 2 is euclidean.\n"<<std::endl;
    std::cout<<"************** Auther: Allen.Lao *************\n"<<std::endl;

}

int main(int argc, char * argv[])
{
    if(argc> 1){
        char action[100];
        sprintf(action, "%s", argv[1]);
        if (strcmp(action, "lda") == 0)
        {
            if (argc == 12)
            {
                int k = atoi(argv[2]);
                int sp = atoi(argv[3]);
                int burnin = atoi(argv[4]);
                int lag = atoi(argv[5]);
                int itv = atoi(argv[6]);
                double alpha = atof(argv[7]);
                double beta = atof(argv[8]);
                char* root = argv[9];
                make_directory(root);
                Corpus cp;
                cp.read_word_seq_data(argv[10]);
                Corpus tt;
                tt.read_word_seq_data(argv[11]);
                LDA_GIBBS lda(k ,sp, burnin, lag, itv,alpha, beta, root, &cp, &tt);
                lda.estimate();
            }
            else if (argc == 11)
            {
                int k = atoi(argv[2]);
                int sp = atoi(argv[3]);
                int burnin = atoi(argv[4]);
                int lag = atoi(argv[5]);
                int itv = atoi(argv[6]);
                double alpha = atof(argv[7]);
                double beta = atof(argv[8]);
                char* root = argv[9];
                make_directory(root);
                Corpus cp;
                cp.read_word_seq_data(argv[10]);
                //Corpus tt;
                //tt.read_word_seq_data(argv[11]);
                LDA_GIBBS lda(k ,sp, burnin, lag, itv,alpha, beta, root, &cp, NULL);
                lda.estimate();
            }
            else
            {
                print_usage();
                return 0;
            }
        }
        else if (strcmp(action, "mlda") == 0)
        {
            int k = atoi(argv[2]);
            int sp = atoi(argv[3]);
            int burnin = atoi(argv[4]);
            int lag = atoi(argv[5]);
            int itv = atoi(argv[6]);
            double alpha = atof(argv[7]);
            double eta = atof(argv[8]);
            char* root = argv[9];
            make_directory(root);
            //languages
            int nlang = atoi(argv[10]);
            if (argc == (11 + nlang))
            {
                const char* files[nlang];
                int l;
                for (l = 0; l < nlang; l++)
                {
                    files[l] = argv[11+l];
                }
                MLCorpus train(nlang);
                train.read_word_seq_data(files);
                std::cout<<"Finished loading data.\n";
                MLDA_GIBBS mlda(k, sp, burnin, lag, itv, alpha, eta, root, &train, NULL);
                mlda.estimate();
            }
            else if (argc == (11 + 2 * nlang))
            {
                const char* train_files[nlang];
                const char* test_files[nlang];
                int l;
                for (l = 0; l < nlang; l++)
                {
                    train_files[l] = argv[11+l];
                }
                MLCorpus train(nlang);
                train.read_word_seq_data(train_files);
                for (l = 0; l < nlang; l++)
                    test_files[l] = argv[11+nlang+l];
                MLCorpus test(nlang);
                test.read_word_seq_data(test_files);
                MLDA_GIBBS mlda(k, sp, burnin, lag, itv, alpha, eta, root, &train,&test);
                mlda.estimate();
            }
            else
            {
                print_usage();
                return 0;
            }
            
        }
        
        else if (strcmp(action, "corr-lda") == 0)
        {
            int k = atoi(argv[2]);
            int sp = atoi(argv[3]);
            int burnin = atoi(argv[4]);
            int lag = atoi(argv[5]);
            int itv = atoi(argv[6]);
            double alpha = atof(argv[7]);
            double eta = atof(argv[8]);
            char* root = argv[9];
            make_directory(root);
            //languages
            int nlang = atoi(argv[10]);
            if (argc == (11 + nlang))
            {
                const char* files[nlang];
                int l;
                for (l = 0; l < nlang; l++)
                {
                    files[l] = argv[11+l];
                }
                MLCorpus train(nlang);
                train.read_word_seq_data(files);
                CORR_LDA_GIBBS clda(k, sp, burnin, lag, itv, alpha, eta, root, &train,NULL);
                clda.estimate();
            }
            else if (argc == (11 + 2 * nlang))
            {
                const char* train_files[nlang];
                const char* test_files[nlang];
                int l;
                for (l = 0; l < nlang; l++)
                {
                    train_files[l] = argv[11+l];
                }
                MLCorpus train(nlang);
                train.read_word_seq_data(train_files);
                for (l = 0; l < nlang; l++)
                {
                    test_files[l] = argv[11+nlang+l];
                }
                MLCorpus test(nlang);
                test.read_word_seq_data(test_files);
                CORR_LDA_GIBBS clda(k, sp, burnin, lag, itv, alpha, eta, root, &train,&test);
                clda.estimate();
            }
            else
            {
                print_usage();
                return 0;
            }
            
        }
        
        else if (strcmp(action, "plda") == 0)
        {
            int k = atoi(argv[2]);
            int fc = atoi(argv[3]);
            int sp = atoi(argv[4]);
            int burnin = atoi(argv[5]);
            int lag = atoi(argv[6]);
            int itv = atoi(argv[7]);
            double alpha = atof(argv[8]);
            double eta = atof(argv[9]);
            double lmd = atof(argv[10]);
            char* root = argv[11];
            make_directory(root);
            //languages
            int nlang = atoi(argv[12]);
            if (argc == (13 + nlang))
            {
                const char* files[nlang];
                int l;
                for (l = 0; l < nlang; l++)
                {
                    files[l] = argv[13+l];
                }
                MLCorpus train(nlang);
                train.read_word_seq_data(files);
                PLDA_GIBBS plda(k, fc, sp, burnin, lag, itv, alpha, eta, lmd,root, &train,NULL);
                plda.estimate();
            }
            else if (argc == (13 + 2 * nlang))
            {
                const char* train_files[nlang];
                const char* test_files[nlang];
                int l;
                for (l = 0; l < nlang; l++)
                {
                    train_files[l] = argv[13+l];
                }
                MLCorpus train(nlang);
                train.read_word_seq_data(train_files);
                for (l = 0; l < nlang; l++)
                {
                    test_files[l] = argv[13+nlang+l];
                }
                MLCorpus test(nlang);
                test.read_word_seq_data(test_files);
                PLDA_GIBBS plda(k, fc, sp, burnin, lag, itv, alpha, eta, lmd,root, &train,&test);
                plda.estimate();
            }
            else
            {
                print_usage();
                return 0;
            }
            
        }
        
        else print_usage();
    }
    else
        print_usage();

    return 0;
}
