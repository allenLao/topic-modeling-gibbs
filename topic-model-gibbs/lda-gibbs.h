//
//  lda-gibbs.h
//  topic-modeling
//
//  Created by allen on 9/23/13.
//  Copyright (c) 2013 allen. All rights reserved.
//

#ifndef __topic_modeling__lda_gibbs__
#define __topic_modeling__lda_gibbs__

#include <stdlib.h>
#include <iostream>
#include <ctime>
#include "data.h"
#include "utils.h"
#include "cokus.h"

#define myrand() (double) (((unsigned long) randomMT()) / 4294967296.)

class LDA_GIBBS
{
    int nTopic; //topic
    int nVocabuary; //vocabulary
    int nDocument; //doc number
    int nIteration; //iteration
    int nBurnin; //throw away first nBurnin iterations
    int nLag; // freq to dump model
    int nInterval; //interval of compute hold-out likelihood
    double alpha;
    double eta;
    char root[100];

    std::vector< std::vector<int> > topic_words;
    std::vector<int> sum_topic_words;
    //topic-word distribution
    std::vector< std::vector<double> > beta;
    
    // topic states, document * words
    std::vector< std::vector<int> > states;
    //topic distribution of a document
    std::vector< std::vector<int> > dtopics;
    
    //document propotion
    std::vector< std::vector<double> > theta;
    Corpus* corpus;
    
    //test data
    Corpus* test;
    std::vector< std::vector<int> > test_states;
    std::vector< std::vector<int> > test_dtopics;   

public:
    LDA_GIBBS(int, int, int, int, int, double,
              double, char*, Corpus*, Corpus*);
    
    ~LDA_GIBBS(){}
    //given a d-th document and w-th word in it,
    //sample a new topic assignment
    int sampling(int d, int w);
    //inference topic given current model
    double inference(int d);
    void estimate();
    void update();
    double holdout_liks();
    double likelihood();
    void init();
    void save(const char* prefix);
    void save_state();
};

#endif /* defined(__topic_modeling__lda_gibbs__) */
