//
//  pmld.cpp
//  topic-model-gibbs
//
//  Created by allen on 10/11/13.
//  Copyright (c) 2013 allen. All rights reserved.
//

#include "pmld.h"

double PLDA_GIBBS::euclidean(double *x, double *y)
{
    double rst = 0;
    int k = 0;
    for (k = 0; k < nTopic; k++)
    {
        rst +=pow(x[k] - y[k], 2);
    }
    return sqrt(rst);
}

double PLDA_GIBBS::kl(double *x, double *y)
{
    double rst = 0;
    int k = 0;
    for (k = 0; k < nTopic; k++)
    {
        rst += x[k]*(log(x[k]/y[k]));
    }
    return rst;
}

double PLDA_GIBBS::hellinger(double *x, double *y)
{
    double rst = 0;
    int k = 0;
    for (k = 0; k < nTopic; k++)
        rst +=pow((sqrt(x[k]) - sqrt(y[k])), 2);
    return rst;
}

double PLDA_GIBBS::js(double *x, double *y)
{
    double avg[nTopic];
    int k;
    for (k = 0; k < nTopic; k++)
        avg[k] = (x[k] + y[k])/2;
    return 0.5 * ( kl(x, avg) + kl(y, avg));
}

double PLDA_GIBBS::myfunction(double *x, double *y)
{
    switch (funct)
    {
        case 1:
            return hellinger(x, y);
        case 2:
            return euclidean(x, y);
        default:
            return js(x, y);
    }
}

void PLDA_GIBBS::estimate()
{
    int t, k, w, l, it = 0, nf=0, lang;
    char prefix[100];
    char likes_file[100];
    sprintf(likes_file, "%s/pmld_%d_likelihood.liks",root, nTopic);
    FILE* likptr = fopen(likes_file, "w");
    time_t begin = time(0);
    std::cout<<"***************Begin to PMLDA-Gibbs **************\n";
    std::cout<<"Begin with:"<<ctime(&begin)<<"\n";
    while (it < nIteration)
    {
        time_t lbegin = time(0);
        for (t = 0; t < train->size(); t++)
        {
            Tuple* tuple = train->tuple(t);
            
            for (l = 0; l< tuple->size(); l++)
            {
                Document doc = tuple->get(l);
                lang = doc.language();
                for (w = 0; w < doc.size(); w++)
                {
                    k = sampling(l, t, w, false);
                    //update parameters
                    //std::cout<<"k:-l "<<k<<" "<<lang<< " "<<doc.word(w)<<std::endl;
                    topic_words[lang][k][doc.word(w)]++;
                    sum_topic_words[lang][k] ++;
                    dtopics[t][lang][k] ++;
                    states[t][lang][w] = k;
                }
            }
        }
        time_t lend = time(0);
        std::cout<<"iteration "<<it<<"-th: "<<difftime(lend, lbegin)<<" seconds"<<std::endl;
        
        if ((it > nBurnin) && (it % nLag == 0))
        {
            // dump model
            update();
            sprintf(prefix, "%04d", nf);
            save(prefix);
            save_state();
            nf++;
        }
        if (test != NULL)
        {
            holdout_liks();
            if ( (it % nInterval == 0))
            {
                time_t hend = time(0);
                //write down hold-out likelihood
                double liks = holdout_liks();
                fprintf(likptr, "%f %f\n", liks, difftime(hend, begin));
                fflush(likptr);
                std::cout<<liks<<" "<<difftime(hend, begin)<<std::endl;
            }
        }
        it++;
    }
    fflush(likptr);
    fclose(likptr);
    update();
    sprintf(prefix, "final");
    save_state();
    save(prefix);
    time_t end = time(0);
    std::cout<<"It takes around "<<difftime(begin, end) / 60<<" mins.\n";
    std::cout<<"End with:"<<ctime(&end)<<"\n";
    std::cout<<"***************End PMLDA-Gibbs **************\n";
    
}

double PLDA_GIBBS::holdout_liks()
{
    if (test != NULL)
    {
        double likes = 0;
        int count = 0;
        int d = 0;
        //compute beta and theta
        for (d = 0; d <test->size(); d++)
        {
            if (test->tuple(d)->size()> 0)
            {
                likes += inference(d);
                count += test->tuple(d)->words();
            }
            
        }
        if (likes > 0 && count > 0)
        {
            return log(likes/count);
        }
        
    }
    return  0;
}

int PLDA_GIBBS::sampling(int lang, int d, int w, bool btest)
{
    int k, l, kk;
    Tuple *tuple;
    Document doc;
    int wid = -1;
    int ct = -1;
    int lid = -1;
    double score[nTopic], mstheta[nTopic], ltheta[nTopic];
    if (!btest)
    {
        tuple = train->tuple(d);
        doc = tuple->get(lang);
        wid = doc.word(w);
        lid = doc.language();
        ct = states[d][lid][w];
        topic_words[lid][ct][wid]--;
        sum_topic_words[lid][ct]--;
        
        dtopics[d][lid][ct]--;
        //local theta
        for (k = 0; k < nTopic; k++)
        {
            mstheta[k] = (dtopics[d][lid][k] + alpha)/(doc.size() - 1 + alpha * nTopic);
        }
    }
    else
    {
        tuple = test->tuple(d);
        doc = tuple->get(lang);
        wid = doc.word(w);
        lid = doc.language();
        ct = test_states[d][lid][w];
        if (ct == -1)
            return -1;
        test_dtopics[d][lid][ct]--;
        for (k = 0; k < nTopic; k++)
            mstheta[k] = (test_dtopics[d][lid][k] + alpha)/(doc.size() - 1 + alpha * nTopic);
    }
    
    for (k = 0; k < nTopic; k++)
    {
        score[k] = (topic_words[lid][k][wid]+eta) / (sum_topic_words[lid][k] + nVocab[lid] * eta);
        if (btest)
        {
            score[k] *= (test_dtopics[d][lid][k] + alpha);
            
//            for (kk = 0; kk < nTopic; kk++)
//                if (k == kk)
//                    stheta[kk] = (test_dtopics[d][lid][k] + alpha + 1)/(doc.size() + alpha * nTopic);
//                else
//                    stheta[kk] = (test_dtopics[d][lid][k] + alpha)/(doc.size() + alpha * nTopic);
            
            //when topic = k
            double tmp = 0;
            for (l = 0; l < tuple->size(); l++)
            {
                int llid = tuple->get(l).language();
                if (l != lang)
                {
                    for (kk = 0; kk < nTopic; kk++)
                        ltheta[kk] = (test_dtopics[d][llid][kk] + alpha)/(tuple->get(l).size() + alpha * nTopic);
                    tmp += (myfunction(mstheta, ltheta) * lambda);

//                    tmp += (myfunction(mstheta, ltheta) * lambda - myfunction(stheta, ltheta) * lambda);
                }
            }
            score[k] *= exp(tmp);
        }
        else
        {
            score[k] *= (dtopics[d][lid][k] + alpha);
//            for (kk = 0; kk < nTopic; kk++)
//                if (k == kk)
//                    stheta[kk] = (dtopics[d][lid][kk] + 1 + alpha)/(doc.size() + alpha * nTopic);
//                else
//                    stheta[kk] = (dtopics[d][lid][kk] + alpha)/(doc.size() + alpha * nTopic);
            
            double tmp = 0;
            for (l = 0; l < tuple->size(); l++)
            {
                int llid = tuple->get(l).language();
                if (l != lang)
                {
                    for (kk = 0; kk < nTopic; kk++)
                        ltheta[kk] = (dtopics[d][llid][kk] + alpha)/(tuple->get(l).size() + alpha * nTopic);
//                    tmp += (myfunction(mstheta, ltheta) - myfunction(stheta, ltheta));
                    tmp += (myfunction(mstheta, ltheta) * lambda);

                }
            }
            score[k]*= exp(tmp);
        }
    }
    //cumulate scores
    for (k = 1; k < nTopic; k++)
        score[k] = score[k-1] + score[k];
    //random select
    double rdm = myrand()*score[nTopic-1];
    k = 0;
    for (k = 0; k < nTopic; k++)
        if (rdm < score[k])
            break;
    return k;
}

//discard the words not in training data
double PLDA_GIBBS::inference(int t)
{
    int l, w, k, lang, count[nLang];
    double lik = 0;
    double local_theta[nLang][nTopic];
    
    Tuple* tuple = test->tuple(t);
    
    //    std::cout<< tuple->size()<<std::endl;
    
    for (l = 0; l< tuple->size(); l++)
    {
        Document doc = tuple->get(l);
        lang = doc.language();
        
        for (w = 0; w < doc.size(); w++)
        {
            k = sampling(l, t, w, true);
            test_dtopics[t][l][k]++;
            test_states[t][lang][w] = k;
        }
        count[l] = doc.size();
    }
    for ( l= 0; l < tuple->size(); l++)
    {
        for (k = 0; k < nTopic; k++)
            local_theta[l][k] = (test_dtopics[t][l][k] + alpha)/(count[l] + alpha*nTopic);
    }
    
    
    lik = 0;
    for (l = 0; l < tuple->size(); l++)
    {
        Document doc = tuple->get(l);
        for (w = 0; w < doc.size(); w++)
        {
            for (k = 0; k < nTopic; k++)
            {
                double tw = (topic_words[doc.language()][k][doc.word(w)] +eta)/(eta*nVocab[doc.language()] + sum_topic_words[doc.language()][k]);
                lik += tw * local_theta[doc.language()][k];
            }
        }
    }
    return (lik);
}

void PLDA_GIBBS::update()
{
    int d, w, k, l;
    for (l = 0; l < nLang; l++)
        for (k = 0; k <nTopic; k++)
            for (w = 0; w < nVocab[l]; w++)
                beta[l][k][w] = (topic_words[l][k][w] + eta)/(sum_topic_words[l][k] + nVocab[l]*eta);
    
    for (d = 0; d < train_docs(); d++)
    {
        Tuple* tuple = train->tuple(d);
        for (l = 0; l < tuple->size(); l++)
        {
            int lan = tuple->get(l).language();
            for (k =0; k <nTopic; k++)
                theta[d][lan][k] = (dtopics[d][lan][k] + alpha)/(train->tuple(d)->words() + nTopic * alpha);
        }
        
    }
}

void PLDA_GIBBS::save_state()
{
    int d, w, l;
    char cst[nLang][100];
    FILE* fptr[nLang];
    for (l = 0; l< nLang; l++)
    {
        sprintf(cst[l], "%s/train_assignment_lang_%d.bat",root, l);
        fptr[l] = fopen(cst[l], "w");
    }
    
    for (d = 0; d < train_docs(); d++)
    {
        Tuple* tuple = train->tuple(d);
        for (l = 0; l < tuple->size(); l++)
        {
            Document doc = tuple->get(l);
            int lang = doc.language();
            fprintf(fptr[lang], "%d:%d ", doc.docid(),doc.size());
            for (w = 0; w < doc.size(); w++)
                fprintf(fptr[lang], "%d ", states[d][lang][w]);
            fprintf(fptr[lang], "\n");
            fflush(fptr[lang]);
            
        }
    }
    for (l = 0; l < nLang; l++)
    {
        fflush(fptr[l]);
        fclose(fptr[l]);
    }
    //save test data
    if (test != NULL){
        for (l = 0; l< nLang; l++)
        {
            sprintf(cst[l], "%s/test_assignment_lang_%d.bat",root, l);
            fptr[l] = fopen(cst[l], "w");
        }
        
        for (d = 0; d < test_docs(); d++)
        {
            Tuple* tuple = test->tuple(d);
            for (l = 0; l < tuple->size(); l++)
            {
                Document doc = tuple->get(l);
                int lang = doc.language();
                fprintf(fptr[lang], "%d:%d ", doc.docid(),doc.size());
                for (w = 0; w < doc.size(); w++)
                    fprintf(fptr[lang], "%d ", test_states[d][lang][w]);
                fprintf(fptr[lang], "\n");
                fflush(fptr[lang]);
                
            }
        }
        for (l = 0; l < nLang; l++)
        {
            fflush(fptr[l]);
            fclose(fptr[l]);
        }
    }
}

void PLDA_GIBBS::save(const char *prefix)
{
    int l, k, w, d;
    char beta_file[100], theta_file[100];
    FILE* betaptr, *thetaptr[nLang];
    
    //keep topic-word distributions
    for (l = 0; l < nLang; l++)
    {
        sprintf(theta_file, "%s/%s_lang_%d.theta",root, prefix, l);
        
        thetaptr[l] = fopen(theta_file, "w");
        sprintf(beta_file, "%s/%s_language_%d.beta",root, prefix,l);
        betaptr = fopen(beta_file, "w");
        
        for (k = 0; k < nTopic; k++)
        {
            for (w = 0; w < nVocab[l]; w++)
                fprintf(betaptr, "%f ", beta[l][k][w]);
            fprintf(betaptr, "\n");
        }
        fflush(betaptr);
        fclose(betaptr);
    }
    
    //
    for (d = 0; d < train->size(); d++)
    {
        Tuple* tuple = train->tuple(d);
        for (l = 0; l < tuple->size(); l++)
        {
            int lan = tuple->get(l).language();
            fprintf(thetaptr[lan], "%d ", d);
            for (k = 0; k < nTopic; k++)
            {
                fprintf(thetaptr[lan], "%f ", theta[d][lan][k]);
            }
            fprintf(thetaptr[lan], "\n");
        }
        
    }
    for (l = 0; l < nLang; l++)
    {
        fflush(thetaptr[l]);
        fclose(thetaptr[l]);
    }
    
}

void PLDA_GIBBS::init()
{
    int k, w, d, l;
    for (l = 0; l < nLang; l++)
    {
        nSize[l] = train_docs();
        nVocab[l] = train->types(l);
        nTotal[l] = train->total(l);
        nMax [l]  = train->maxs(l);
        if (test!=NULL)
        {
            nTestTotal[l] = test->total(l);
            nTestMax[l] = test->maxs(l);
        }
    }
    
    //zero topic-word counts
    for (l = 0; l < nLang; l++)
    {
        std::vector<std::vector<int> > ltopic;
        std::vector<std::vector<double> > lbeta;
        std::vector<int> stopic(nTopic, 0);
        sum_topic_words.push_back(stopic);
        for (k = 0; k < nTopic; k++)
        {
            std::vector<int> swt(nVocab[l], 0);
            ltopic.push_back(swt);
            std::vector<double> sbt(nVocab[l], 0.0);
            lbeta.push_back(sbt);
        }
        topic_words.push_back(ltopic);
        sum_topic_words.push_back(stopic);
        beta.push_back(lbeta);
    }
    
    //random initialize
    
    for (d = 0; d < train->size(); d++)
    {
        std::vector<std::vector<int> > dls;
        std::vector<std::vector<int> > dds; //local topic count
        std::vector<std::vector<double> > ddt; //local theta
        
        Tuple* tuple = train->tuple(d);
        for (l =0; l < nLang; l++)
        {
            std::vector<int> lls;
            dls.push_back(lls);
            std::vector<int> localdd;
            dds.push_back(localdd);
            std::vector<double> localtheta;
            ddt.push_back(localtheta);
        }
        
        for (l =0; l < train->tuple(d)->size(); l++)
        {
            Document doc = tuple->get(l);
            int lang = doc.language();
            dls[lang].resize(doc.size(), 0);
            dds[lang].resize(nTopic, 0);
            ddt[lang].resize(nTopic, 0);
        }
        states.push_back(dls);
        dtopics.push_back(dds);
        theta.push_back(ddt);
        
        for (l =0; l < train->tuple(d)->size(); l++)
        {
            Document doc = tuple->get(l);
            int lang = doc.language();
            for (w = 0; w < doc.size(); w++)
            {
                int t =(int) (myrand() * nTopic);
                states[d][lang][w] = t;
                topic_words[lang][t][doc.word(w)]++;
                sum_topic_words[lang][t]++;
                dtopics[d][lang][t]++;
            }
        }
    }
    update();
    
    if (test != NULL)
    {
        for (d = 0; d < test->size(); d++)
        {
            Tuple* ttuple = test->tuple(d);
            std::vector<std::vector<int> > ttd;
            std::vector<std::vector<int> > dls;
            std::vector<std::vector<double> > ddt; //local theta

            for (l =0; l < nLang; l++)
            {
                std::vector<int> lls;
                dls.push_back(lls);
                std::vector<int> llt;
                ttd.push_back(llt);
                std::vector<double> localtheta;
                ddt.push_back(localtheta);
            }
            
            for (l = 0; l <ttuple->size(); l++)
            {
                Document doc = ttuple->get(l);
                int lans = doc.language();
                dls[lans].resize(doc.size(), 0);
                ttd[lans].resize(nTopic, 0);
            }
            test_dtopics.push_back(ttd);
            test_states.push_back(dls);
            test_theta.push_back(ddt);
            for (l = 0; l <ttuple->size(); l++)
            {
                Document doc = ttuple->get(l);
                int lans = doc.language();
                for (w = 0; w < doc.size(); w++)
                {
                    if (doc.word(w) >= nVocab[doc.language()])
                        test_states[d][lans][w] = -1;
                    int t =(int) (myrand() * nTopic);
                    test_dtopics[d][lans][t]++;
                    test_states[d][lans][w] = t;
                }
                //compute theta
                
            }
        }
    }
}

PLDA_GIBBS::PLDA_GIBBS(int k, int fc, int s, int bn, int lg, int itv, double ap,
           double et, double lbda, char* rt, MLCorpus* cp, MLCorpus* tt)
{
    nTopic = k;
    funct = fc;
    nIteration = s;
    nBurnin = bn;
    nLag = lg;
    nInterval = itv;
    alpha = ap;
    eta = et;
    lambda = lbda;
    train = cp;
    nLang = cp->languages();
    test = tt;
    sprintf(root, "%s", rt);
    sprintf(root, "%s", rt);
    nSize.resize(nLang, 0);
    nVocab.resize(nLang, 0);
    nTotal.resize(nLang, 0);
    nMax.resize(nLang, 0);
    nTestMax.resize(nLang, 0);
    nTestSize.resize(nLang, 0);
    nTestTotal.resize(nLang, 0);
    init();
}
PLDA_GIBBS::~PLDA_GIBBS()
{
    int d, k, l;
    //release mems
    for (l = 0; l < nLang; l++)
    {
        for (k = 0; k < nTopic; k++)
        {
            beta[l][k].clear();
            topic_words[l][k].clear();
        }
        beta[l].clear();
        topic_words[l].clear();
        sum_topic_words[l].clear();
    }
    beta.clear();
    topic_words.clear();
    sum_topic_words.clear();
    //
    for (d = 0; d < train_docs(); d++)
    {
        for(l = 0; l < train->tuple(d)->size(); l++)
            states[d][l].clear();
        dtopics[d].clear();
        states[d].clear();
        theta[d].clear();
    }
    states.clear();
    dtopics.clear();
    theta.clear();
    if(test != NULL)
    {
        for (d = 0; d < test_docs(); d++)
        {
            test_dtopics[d].clear();
            test_states[d].clear();
        }
        test_dtopics.clear();
        test_states.clear();
    }
    nSize.clear();
    nVocab.clear();
    nTotal.clear();
    nMax.clear();
    nTestMax.clear();
    nTestSize.clear();
    nTestTotal.clear();
}